<?php if ($is_logged_in): ?>
<h2>You are logged in</h2>
<a href="/auth0/logout">Logout</a>
<?php else: ?>
<h2>You are not logged in</h2>
<a href="/auth0/login">Login</a>
<?php endif; ?>



