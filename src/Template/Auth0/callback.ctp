<?php if(!$userInfo): ?>
    <h2>You are not logged in</h2>
    <a href="/auth0/login">Login</a>
<?php else: ?>
    <?php printf( 'Hello %s!', htmlspecialchars( $userInfo['name'] ) ); ?>
    
    
    <div class="card" style="width: 18rem;">
        
        <img class="card-img-top" src='<?php echo $userInfo["picture"] ?>'alt="Card image cap">

        <ul class="list-group list-group-flush">
            <li class="list-group-item">Name: <?php echo $userInfo["nickname"] ?></li>
            <li class="list-group-item">Email: <?php echo $userInfo['name'] ?></li>
            <li class="list-group-item">Sub: <?php echo $userInfo['sub'] ?></li>
        </ul>


    </div>
    
    <hr>

    
    <a href="/auth0/logout">Logout</a>
<?php endif; ?>

