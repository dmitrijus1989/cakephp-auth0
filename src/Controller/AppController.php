<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Auth0\SDK\Auth0;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
//        $this->loadComponent('Auth', [
//            'loginAction' => [
//                'controller' => 'Users',
//                'action' => 'login',
//            ],
//            'authError' => 'Did you really think you are allowed to see that?',
//            'authenticate' => [
//                'Form' => [
//                    'fields' => ['username' => 'email', 'password' => 'password']
//                ]
//            ],
//            'storage' => 'Session'
//        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        
        
        $auth0 = $this->getAuth();
        $userInfo = $auth0->getUser();
        
        if (!$userInfo) {
            $is_logged_in = false;
        } else {
            $is_logged_in = true;
        }
        
        $this->set(compact('is_logged_in', 'userInfo'));  

    }
    
    public function getAuth() {
        $auth0 = new Auth0([
        'domain' => 'ravcake.eu.auth0.com',
        'client_id' => '42UdlJjeXAX7z3zRlsk12TzTpXOLudvQ',
        'client_secret' => 'c_EBQzRRAQkWDbxDaDJOySFbRsZutiVuV1lbI5ErBVZ_ycQCZ-v7VL7Cg1xha_MC',
        'redirect_uri' => 'http://dmitrij.testuojam.com/auth0/callback',
        'persist_id_token' => true,
        'persist_access_token' => true,
        'persist_refresh_token' => true,
      ]);
       return $auth0;
    }
    

}
