<?php

namespace App\Controller;

use App\Controller\AppController;
use Auth0\SDK\Auth0;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class Auth0Controller extends AppController {
    
    public function index() {
        
    }
    
    public function login() {
        $auth0 = new Auth0([
        'domain' => 'ravcake.eu.auth0.com',
        'client_id' => '42UdlJjeXAX7z3zRlsk12TzTpXOLudvQ',
        'client_secret' => 'c_EBQzRRAQkWDbxDaDJOySFbRsZutiVuV1lbI5ErBVZ_ycQCZ-v7VL7Cg1xha_MC',
        'redirect_uri' => 'http://dmitrij.testuojam.com/auth0/callback',
        'scope' => 'openid profile email',
      ]);

      $auth0->login();        
    }
    public function callback() {
        $auth0 = $this->getAuth();
        $userInfo = $auth0->getUser();

        if (!$userInfo) {
            // We have no user info
            // See below for how to add a login link
            var_dump($userInfo);
            var_dump('</br>');
            var_dump('We have no user info');
        } else {
            // User is authenticated
            // See below for how to display user information
//            var_dump($userInfo);
//            var_dump('</br>');
//            var_dump('User is authenticated');
//            var_dump($arr);
        }
        $this->set(compact('userInfo'));
    }
    public function logout() {
        $auth0 = $this->getAuth();
        $auth0->logout();
        $return_to = 'http://' . $_SERVER['HTTP_HOST'];
        $logout_url = sprintf('http://%s/v2/logout?client_id=%s&returnTo=%s', 'ravcake.eu.auth0.com', '42UdlJjeXAX7z3zRlsk12TzTpXOLudvQ', $return_to);
        header('Location: ' . 'http://dmitrij.testuojam.com/auth0');
        die();
    }
    

}